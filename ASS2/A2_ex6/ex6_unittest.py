"""
Author -- Michael Widrich
Contact -- widrich@ml.jku.at
Date -- 01.10.2019

###############################################################################

The following copyright statement applies to all code within this file.

Copyright statement:
This  material,  no  matter  whether  in  printed  or  electronic  form,
may  be  used  for personal  and non-commercial educational use only.
Any reproduction of this manuscript, no matter whether as a whole or in parts,
no matter whether in printed or in electronic form, requires explicit prior
acceptance of the authors.

###############################################################################
"""

import os
import sys
import itertools
from glob import glob
import types
import hashlib

ex_file = 'ex6.py'
full_points = 4
points = full_points
python = sys.executable

directories = sorted(glob(os.path.join("ex6_testfiles", "*")))
if not len(directories):
    raise FileNotFoundError("Could not find files in directory ex6_testfiles")
if len(directories) != 4:
    raise FileNotFoundError(f"Expected 4 folders in ex6_testfiles, found {len(directories)}")

inputs = directories

correct_outs = [(['correct_0.raw.seq', 'correct_1.raw.seq', 'correct_2.raw.seq', 'correct_3.raw.seq',
                  'correct_4.raw.seq', 'correct_5.raw.seq', 'correct_6.raw.seq', 'correct_7.raw.seq'],
                 'fc734af01e87fde2ad177f4f9d4d4f68'),
                (['correct_0.raw.seq', 'correct_1.raw.seq', 'correct_2.raw.seq', 'correct_3.raw.seq',
                  'correct_4.raw.seq', 'correct_5.raw.seq', 'correct_6.raw.seq'],
                 '8306a85822661688bd3b45146959b64d'),
                ([],
                 'd41d8cd98f00b204e9800998ecf8427e'),
                (['correct_in_a_subfolder.raw.seq', 'correct_0.raw.seq', 'correct_1.raw.seq', 'correct_2.raw.seq',
                  'correct_3.raw.seq', 'correct_4.raw.seq', 'correct_5.raw.seq', 'correct_6.raw.seq',
                  'correct_7.raw.seq', 'correct_8.raw.seq', 'correct_9.raw.seq', 'correct_10.raw.seq'],
                 '70955d5be0b03b086d85943cb9249206')]


outs = []
outs += [f"Unittest for: {ex_file}"]

for test_i, test_params in enumerate(zip(inputs, correct_outs)):
    directories, correct_out = test_params
    correct_filepaths = correct_out[0]
    correct_filecontents = correct_out[1]
    filepaths = None
    checksum = None
    comments = ''
    
    try:
        from ex6 import get_hamsters
        generator = get_hamsters(folderpath=directories)
        errs = ''
        if not isinstance(generator, types.GeneratorType):
            points -= full_points / len(inputs)
            comments = f"not a generator object but {type(generator)} object"
        else:
            generator = list(generator)
            if not len(generator):
                filepaths, filecontents = [], []
            else:
                filepaths, filecontents = zip(*[(fp, fc) for fp, fc in generator])
            checksum = hashlib.md5()
            _ = [checksum.update(bytes(c, 'utf-8')) for c in filecontents]
            checksum = checksum.hexdigest()
            if checksum != correct_filecontents:
                points -= full_points / len(inputs) / 3
            if any([rfp != cfp for rfp, cfp in zip(filepaths, correct_filepaths)]):
                points -= full_points / len(inputs) / (3 / 2)
            
    except Exception as e:
        errs = e
        points -= full_points / len(inputs)
    
    outs += ["#" * 10]
    outs += [f"Test {test_i}"]
    outs += ["#" * 10]
    outs += [f"Input was:\n---\nfoldername = '{directories}'\n---\n"]
    outs += [f"Error messages:\n---\n{errs}\n---\n"]
    outs += [f"Returned files were:\n---\n{filepaths}\n---\n"]
    outs += [f"Returned files should be:\n---\n{correct_filepaths}\n---\n"]
    outs += [f"File contents matched:\n---\n{checksum == correct_filecontents}\n---\n"]
    outs += [f"Comments:\n---\n{comments}\n---\n"]
    outs += [f"Current points:{points:.2f}"]

points = points if points > 0 else 0
outs += ["#" * 10]
outs += [f"Estimated points upon submission: {points:.2f}"]
outs += [f"This is only an estimate, see Instructions for submitting homework in moodle for common mistakes "
         f"that can still lead to 0 points."]
print('\n'.join(outs))
